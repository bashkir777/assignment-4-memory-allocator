#include "mem.h"
#include <assert.h>
#include <stdio.h>



void test_1() {
    void *heap = heap_init(0);
    printf("Testing memory allocation\n");
    debug_heap(stdout, heap);
    void *mem1 = _malloc(0);
    assert(mem1 != NULL);
    debug_heap(stdout, heap);
    _free(mem1);
    debug_heap(stdout, heap);
    heap_term();
    printf("Passed\n");
}

void test_2() {
    printf("Testing one block freeing\n");
    void *heap = heap_init(0);
    printf("Before:");
    debug_heap(stdout, heap);
    void *first = _malloc(256);
    printf("State after:\n");
    debug_heap(stdout, heap);
    assert(first != NULL);
    _free(first);
    printf("After freeing:");
    debug_heap(stdout, heap);
    heap_term();
    printf("Passed\n");
}

static void test_3() {
    printf("Testing three block freeing\n");
    void *heap = heap_init(0);
    printf("Before:\n");
    debug_heap(stdout, heap);

    void *first = _malloc(256);
    void *second = _malloc(256);
    void *third = _malloc(256);

    printf("State after:\n");
    debug_heap(stdout, heap);

    assert(first != NULL);
    assert(second != NULL);
    assert(third != NULL);

    _free(first);
    _free(second);
    _free(third);

    printf("After freeing:\n");
    debug_heap(stdout, heap);
    heap_term();
    printf("Passed\n");
}

void test_4() {
    printf("Testing memory expansion\n");
    void* mem1 = _malloc(100000);
    assert(mem1 != NULL);
    _free(mem1);
    printf("Passed\n");
}


void test_5() {
    printf("Testing ran out of memory, the old memory region cannot be expanded due to a different allocated address range, the new region is allocated somewhere else\n ");
    void *allocated = mmap((void *) HEAP_START, 10, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, -1, 0);
    if (allocated == MAP_FAILED) {
        printf("unable to pre-allocate memory\n");
        return;
    }

    void *allocated_filled_ptr = _malloc(10);
    if (!allocated_filled_ptr) {
        printf("_malloc failed to allocate memory\n");
        return;
    }

    if (allocated == allocated_filled_ptr) {
        printf("memory regions are unexpectedly the same\n");
        return;
    }
    heap_term();
    printf("Passed\n");
}






int main() {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    return 1;
}
